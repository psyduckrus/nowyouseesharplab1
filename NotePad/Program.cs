﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;


namespace NotePad
{
    public class Notebook
    {
        public static Dictionary<int, Note> notebook = new Dictionary<int, Note>();
        static List<string> checkEdit = new List<string>() { "ФАМИЛИЯ", "ИМЯ", "СТРАНА", "НОМЕР", "ДАТА РОЖДЕНИЯ", "ОРГАНИЗАЦИЯ", "ДОЛЖНОСТЬ", "ОТЧЕСТВО", "ЗАМЕТКА" };
        static List<string> checkOperation = new List<string>() { "СОЗДАТЬ", "УДАЛИТЬ", "ИЗМЕНИТЬ", "ПОИСК", "СПИСОК", "ХВАТИТ" };
        static TextInfo myTI = new CultureInfo("ru-RU", false).TextInfo;
        static void Main(string[] args)
        {
            Console.WriteLine("Добро пожаловать в записную книжку!");
            Console.WriteLine("На данный момент я только восстановилась от амнезии, так что я не храню никаких данных %(");
            Console.WriteLine("Желаете создать новый контакт, дабы мне не было грустно?");
            Console.WriteLine("Мои функции: \"СОЗДАТЬ\" \"УДАЛИТЬ\" \"ИЗМЕНИТЬ\" \"ПОИСК\" \"СПИСОК\"");
            Console.WriteLine("Напишите \"ХВАТИТ\" и я навсегда исчезну из вашей жизни.");
            while (true)
            {
                string input = Console.ReadLine();
                while (!(checkOperation.Contains(input.ToUpper())))
                {
                    Console.WriteLine("Что-что ?");
                    Console.WriteLine("Напоминаю свои функции: \"СОЗДАТЬ\" \"УДАЛИТЬ\" \"ИЗМЕНИТЬ\" \"ПОИСК\" \"СПИСОК\" \"ХВАТИТ\"");
                    Console.WriteLine("Введите желаемую функцию:");
                    input = Console.ReadLine();
                }
                if (input.ToUpper() == "ХВАТИТ")
                {
                    Console.Clear();
                    Console.WriteLine("Пока-пока");
                    break;
                }
                if (input.ToUpper() == "СОЗДАТЬ")
                {
                    CreateNewNote();
                    Console.WriteLine("Введите желаемую функцию:");
                }
                if (input.ToUpper() == "ИЗМЕНИТЬ")
                {
                    if (notebook.Count == 0)
                    {
                        Console.WriteLine("Записей пока нет!");
                    }
                    else
                    {
                        Console.WriteLine("Введите ID записи, которую хотите изменить:");
                        EditNote(notebook[SearchForNote()]);
                    }
                    Console.WriteLine("Введите желаемую функцию:");

                }
                if (input.ToUpper() == "СПИСОК")
                {
                    if (notebook.Count == 0)
                    {
                        Console.WriteLine("Записей пока нет!");
                    }
                    else ShowAllNotes();
                    Console.WriteLine("Введите желаемую функцию:");
                }
                if (input.ToUpper() == "ПОИСК") 
                {
                    if (notebook.Count == 0)
                    {
                        Console.WriteLine("Записей пока нет!");
                    }
                        else ReadNote();
                    Console.WriteLine("Введите желаемую функцию:");
                }
                if (input.ToUpper() == "УДАЛИТЬ")
                {
                    if (notebook.Count == 0)
                    {
                        Console.WriteLine("Записей пока нет!");
                    }
                    else DeleteNote();
                    Console.WriteLine("Введите желаемую функцию:");
                }
            }
        }

        public static void CreateNewNote()
        {
            Note note = new Note();
            notebook.Add(note.Id, note);
            string input;
            Console.WriteLine("Окей, теперь нам нужно заполнить обязательные поля.");
            note.Surname = CheckString("ФАМИЛИЯ");
            note.Name = CheckString("ИМЯ");
            note.Country = CheckString("СТРАНА");
            Console.WriteLine("Введите номер:");
            input = Console.ReadLine();
            while (input.Length < 3 | input == "")
            {
                Console.WriteLine("Маловато для номера, нет? Попробуйте снова:");
                input = Console.ReadLine();
            }
            for (int i = 0; i < input.Length; i++)
            {
                if (!Char.IsDigit(input[i]))
                {
                    Console.WriteLine("А это точно номер? Давайте попробуем снова:");
                    input = Console.ReadLine();
                    i = 0;
                }
            }
            note.Number = Convert.ToInt64(input);
            Console.WriteLine($"Контакт создан и ему присвоен ID - {note.Id}!");
        }
        public static void EditNote(Note note)
        {

            Console.WriteLine("Что изменить?\nВведите \"Фамилия\" / \"Имя\" / \"Страна\" / \"Номер\" / \"Дата рождения\" / \"Организация\" / \"Должность\" / \"Отчество\" / \"Заметка\"");
            string input = Console.ReadLine();
            while (!(checkEdit.Contains(input.ToUpper())))
            {
                Console.WriteLine("Что-что ? Еще раз, какое поле нужно изменить:");
                input = Console.ReadLine();
            }
            if (input.ToUpper() == "ФАМИЛИЯ")
            {
                note.Surname = CheckString(input);
            }
            if (input.ToUpper() == "ИМЯ")
            {

                note.Name = CheckString(input); ;
            }
            if (input.ToUpper() == "СТРАНА")
            {
                note.Country = CheckString(input);
            }
            if (input.ToUpper() == "НОМЕР")
            {
                Console.WriteLine("Введите номер:");
                input = Console.ReadLine();
                while (input.Length < 3)
                {
                    Console.WriteLine("Маловато для номера, нет? Попробуйте снова:");
                    input = Console.ReadLine();
                }
                for (int i = 0; i < input.Length; i++)
                {
                    if (!Char.IsDigit(input[i]))
                    {
                        Console.WriteLine("А это точно номер? Давайте попробуем снова:");
                        input = Console.ReadLine();
                        i = 0;
                    }
                }
                note.Number = Convert.ToInt64(input);
            }
            if (input.ToUpper() == "ОТЧЕСТВО")
            {
                note.Fathersname = CheckString(input);
            }
            if (input.ToUpper() == "ОРГАНИЗАЦИЯ")
            {
                note.Org = CheckString(input);
            }
            if (input.ToUpper() == "ДОЛЖНОСТЬ")
            {

                note.OrgPosition = CheckString(input);
            }
            if (input.ToUpper() == "ЗАМЕТКА")
            {
                Console.WriteLine("Введите заметку:");
                input = Console.ReadLine();
                note.Notes = input;
            }
            if (input.ToUpper() == "ДАТА РОЖДЕНИЯ")
            {
                DateTime date;
                Console.WriteLine("Введите дату рождения в формате dd.mm.yyyy:");
                input = Console.ReadLine();
                while (true)
                {
                    try
                    {
                        date = new DateTime(Convert.ToInt32(input.Split('.')[2]), Convert.ToInt32(input.Split('.')[1]), Convert.ToInt32(input.Split('.')[0]));
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("А это точно дата рождения? Давайте попробуем снова:");
                        input = Console.ReadLine();
                        continue;
                    }
                    note.DateOfBirth = date;
                    break;
                }
            }
            Console.WriteLine("Успешно!");
        }
        public static void DeleteNote()
        {
            Console.WriteLine("Введите ID записи, которую хотите удалить:");
            notebook.Remove(SearchForNote());
            Console.WriteLine("Успешно!");
        }
        public static void ReadNote()
        {
            Console.WriteLine("Введите ID записи, которую хотите вывести на экран:");
            Console.WriteLine(notebook[SearchForNote()]);
            Console.WriteLine("Успешно!");

        }
        public static void ShowAllNotes()
        {
            if (notebook.Count > 0)
            {
                foreach (var item in notebook)
                {
                    Console.WriteLine(Note.ToShortString(item.Value));
                }
                Console.WriteLine("Успешно!");
            }
            else Console.WriteLine("Пока что здесь пусто!");
        }
        public static int SearchForNote()
        {
            string input = Console.ReadLine();
            bool format = false;
            bool id = false;
            while (true)
            {
                while (format == false)
                {
                    try
                    {
                        int a = Convert.ToInt32(input);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine($"А это точно ID? Давайте попробуем снова:");
                        input = Console.ReadLine();
                        id = false;
                        format = false;
                        continue;
                    }
                    format = true;
                    break;
                }
                while (format == true & id == false)
                {
                    if (!notebook.ContainsKey(Convert.ToInt32(input)))
                    {
                        Console.WriteLine("Такого ID нет :( Попробуем еще раз:");
                        input = Console.ReadLine();
                        format = false;
                        id = false;
                        continue;
                    }
                    id = true;
                    break;
                }
                if (format == true & id == true) return Convert.ToInt32(input);
            }
        }
        public static string CheckString(string input)
        {
            string field = input.ToLower();
            int spacerCounter = 0;
            int nonletterCounter = 0;
            bool formatCheck, spacerCheck, emptyCheck;
            Console.WriteLine($"{myTI.ToTitleCase(field)} ожидает вашего ввода:");
            input = Console.ReadLine();
            while (true)
            {
                spacerCounter = 0;
                nonletterCounter = 0;
                if (input.Length < 1)
                {
                    emptyCheck = false;
                    spacerCheck = false;
                    formatCheck = false;
                    Console.WriteLine($"А это точно {field.ToLower()}? Давайте попробуем снова:");
                    input = Console.ReadLine();
                    continue;
                }
                emptyCheck = true;
                for (int i = 0; i < input.Length; i++)
                {
                    if (input[i] == ' ') spacerCounter++;
                }
                if (spacerCounter == input.Length)
                {
                    emptyCheck = false;
                    spacerCheck = false;
                    formatCheck = false;
                    Console.WriteLine($"А это точно {field.ToLower()}? Давайте попробуем снова:");
                    input = Console.ReadLine();
                    continue;
                }
                spacerCheck = true;
                for (int i = 0; i < input.Length; i++)
                {
                    if (!Char.IsLetter(input[i]))
                    {
                        nonletterCounter++;
                        break;
                    }
                }
                if (nonletterCounter > 0)
                {
                    emptyCheck = false;
                    spacerCheck = false;
                    formatCheck = false;
                    Console.WriteLine($"А это точно {field.ToLower()}? Давайте попробуем снова:");
                    input = Console.ReadLine();
                    continue;
                }
                formatCheck = true;
                if (emptyCheck & spacerCheck & formatCheck) break;
            }
            return input;
        }
    }
    public class Note
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Fathersname { get; set; } = "не указано";
        public string Org { get; set; } = "не указана";
        public string Country { get; set; }
        public string OrgPosition { get; set; } = "не указана";
        public string Notes { get; set; } = "отсутствуют";
        public DateTime DateOfBirth { get; set; }
        public long Number { get; set; }
        public int Id { get; set; }
        private static int id = 0;

        public override string ToString()
        {
            string s = ($"ID: {Id} - Имя: {Name} - Фамилия: {Surname} - Номер:: {Number} - Страна: {Country} ");
            if (Fathersname != "") s += ("- Отчество: " + Fathersname + " ");
            if (Org != "") s += ("- Организация: " + Org + " ");
            if (Notes != "") s += ("- Заметки: " + Notes + " ");
            if (OrgPosition != "") s += ("- Должность: " + OrgPosition + " ");
            if (!DateOfBirth.Equals(DateTime.MinValue)) s += ("- Дата рождения: " + DateOfBirth.ToShortDateString() + " ");
            else s += ("- Дата рождения: не указана");
            return s;
        }
        public static string ToShortString(Note note)
        {
            return ($"ID: {note.Id} - Имя: {note.Name} - Фамилия: {note.Surname} - Номер: {note.Number} - Страна: {note.Country}");
        }
        public Note()
        {
            id++;
            Id = id;
        }

    }
}
